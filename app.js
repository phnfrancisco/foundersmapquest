/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');
const http = require('http');
const port = process.env.OPENSHIFT_NODEJS_PORT || '5000';
const ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
let app = express();
let logger = morgan;

app.use(favicon(path.join(__dirname, 'www/images', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'www')));

if (app.get('env') == 'production') {
    app.use(logger('common', { skip: function(req, res) { return res.statusCode < 300; }}));
} else {
    app.use(logger('dev'));
}

let debug = require('debug')('foundersMapQuest:server');

app.set('port', port);

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    if ( app.get('env') === 'development' ) {
        res.send({
            message: err.message,
            error: err
        });
    } else {
        res.redirect('/');
        next(err);
    }
});

let server = http.createServer(app);
server.listen(port, ip);
server.on('error', onError);
server.on('listening', () => {
    let addr = server.address();
    let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    debug('Listening on ' + bind);
});

function onError(error) {
    if (error.syscall !== 'listen') throw error;

    let bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}