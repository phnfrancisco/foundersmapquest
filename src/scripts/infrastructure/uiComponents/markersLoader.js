/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

const template = require('../templateHandler');
const CustomPromise = require('../customPromise');
const config = require('../config');
const $ = global.$;

function markersLoader() {
    let events = {};
    let promise = new CustomPromise();
    let dataGrid = {};
    var controls = {
        csvContentElement: '',
        show: () => {
            dispatch('show');
            $(controls.tpl).modal('show');
            return controls;
        },
        hide: () => $(controls.tpl).modal('hide'),
        on: on,
        off: off,
        tpl: {},
        val: {
            set: newValue => controls.csvContentElement.val(newValue).change(),
            get: () => controls.csvContentElement.val()
        },
        dataGrid: {
            set: data => dataGrid = data,
            get: () => dataGrid
        }
    };

    function dispatch(event, data) {
        if ( !event || !events[event] ) return;
        events[event].forEach( callback => { if( typeof callback === 'function' ) callback(data); });
        return controls;
    }

    function on(event, callback) {
        if ( !events[event] ) events[event] = [];
        events[event].push(callback);
        return controls;
    }

    function off(event) {
        if ( events[event] ) delete events[event];
    }

    function separatorSuggestion(content) {
        let mostUsedSeparator = '',
            mostUsedTimes = 0,
            times = 0;

        [',',';','\t'].forEach( separator => {
            let matches = content.match(new RegExp(separator, "g"));
            if ( matches && ( (times = matches.length) > mostUsedTimes) ) {
                mostUsedTimes = times;
                mostUsedSeparator = separator;
            }
        });

        return mostUsedSeparator;
    }

    function applyBinds() {
        let csvContentElement = controls.csvContentElement = $('#csvContent');

        $('.nav-tabs li a').on('shown.bs.tab', function(evt){
            const actionButton = $(controls.tpl).find('.action');
            switch (evt.target.hash) {
                case '#load' :
                    csvContentElement.on('change', evt => {
                        actionButton[evt.target.value.length ? 'removeClass' : 'addClass']('disabled');
                    });
                    actionButton
                        .off('click')
                        .on('click', () => {
                            if ( !$('#csvContent').val().length ) return;
                            var obj = {
                                csv: controls.val.get(),
                                separator: $('[name=separator]:checked').val(),
                                dataGrid: controls.dataGrid
                            };
                            dispatch('loadData', obj);
                        })
                        .text('Load Data!');
                    break;
                case '#manage':

                    dispatch('manage', controls.dataGrid);

                    actionButton
                        .off('click')
                        .on('click', () => {
                            dispatch('applyMarkers', controls.dataGrid);
                        }).text('Apply Markers!');
                    break;
                default:
                    break;
            }
        });

        csvContentElement.on('change', evt => {
            if ( evt.target.value !== controls.val.get() ) controls.val.set(evt.target.value);
            $(controls.tpl).find('.action').attr('disabled', !evt.target.value);
            let sep = separatorSuggestion(evt.target.value);
            $(`input:radio[name=separator][value="${sep}"]`).click();
        } );


        $(controls.tpl).find('input[type=file]').on('change', event => {
            $(controls.tpl).find('.form-group.file-wrapper').removeClass('has-error').find('.help-block').text('');
            $('#uploadCsvFileFake').val(event.currentTarget.files[0].name);
            try {
                uploadFile(event.currentTarget.files[0]);
            } catch(e) {
                $(controls.tpl)
                    .find('.form-group.file-wrapper')
                    .addClass('has-error')
                    .find('.help-block')
                    .text(e);
            }
        } );
    }

    function progressBar() {
        const barWrapper = controls.tpl.find('.progress');
        const bar = controls.tpl.find('.progress-bar');

        function update(percentage) {
            percentage += '%';
            bar.css({width: percentage});
            bar.text(percentage);
        }

        return {
            show: () => barWrapper.fadeIn(),
            hide: () => barWrapper.fadeOut(),
            update: update
        };
    }

    function labelHandler() {
        const label = $('#uploadCsvFileLabel');
        let prevLabel = label.text();
        var abortBtn = document.createElement('a');
        abortBtn.textContent = '(abort)';

        return {
            rollback: () => label.text(prevLabel),
            set: (fileName, cb) => {
                abortBtn.onclick = () => cb;
                label.html('Uploading file: ' + fileName + ' - ').append(abortBtn);
            }
        };
    }

    function uploadFile(file) {
        if ( file.size > config.get('csvMaxSize') ) {
            throw Error('File exceeds the max size of ' + config.get('csvMaxSize'));
        } else if ( !file.name.match(/^.*\.((c|t)sv|txt)$/i) ) {
            throw Error('File must be in .csv, .tsv or .txt');
        }

        var reader = new FileReader();
        let progress = progressBar();
        let label = labelHandler();

        label.set(file.name, reader.abort);

        reader.readAsText(file);
        reader.onloadstart = () => progress.show();
        reader.onloadend = () => {
            label.rollback();
            progress.hide();
        };
        reader.onprogress = prog => {
            if (prog.lengthComputable) progress.update(Math.round((prog.loaded * 100) / prog.total));
        };
        reader.onload = evt => controls.val.set(evt.target.result);
    }

    template.get('markers')
        .then( tpl => {
            $('body').append(controls.tpl = $(tpl));
            applyBinds();
            $('.nav-tabs li a:first').tab('show');
            promise.resolve(controls);
        } )
        .catch( e => {
            console.log(e);
        } );

    return controls;
}

module.exports = markersLoader;