/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

const template = require('../templateHandler');
const config = require('../config');
const $ = global.$;

function dataTable(gridData) {
    let options = {
        deferRender: true,
        autoWidth: true
    };
    let inner = {
        tpl: {
            tableElement : $('#markersTable')
        },
        tableApi: {}
    };

    //Destroy table if it already exists
    if ( $.fn.dataTable.isDataTable(inner.tpl.tableElement) ) {
        inner.tpl.tableElement.DataTable().destroy();
        $('#manage').empty();
    }

    function getCustomColDefinition(col) {
        let defs = {};
        switch(col) {
            case 'visible':
                defs.title = "";
                defs.data = col;
                defs.className = 'visibleCol';
                defs.render = (data, type, full) => {
                    if ( type === 'sort' ) {
                        return full.visible ? 1 : 0;
                    }
                    return `<a href="#" class="toggleVisibility"><span class="glyphicon glyphicon-eye-${full.visible ? 'open' : 'close'}"></span></a>`;
                };
                break;

            default:
                defs.title = `<span class="title">${col}</span>`;
                defs.data = col;
                defs.className = col.replace(/\s/g, "") + 'Col';
                defs.render = (data, type, full) => {
                    data = full.markerData[col];
                    data = data.trim();
                    let parsedData = '';

                    if ( data.match(/(https?:\/\/.*\.(?:png|jpg|gif))/i) ) { //Match for image
                        parsedData += `<img src="${data}" width="${config.get('gridImageSize').width}"
                                            height="${config.get('gridImageSize').height}" alt="${data.split('/').pop()}" />`;
                    } else if ( data.match(/(https?:\/\/.*\.)/i) ) { //Match for url
                        parsedData +=   `<a href="${data}" target="_blank">
                                            ${data.split('/').pop()}<span class="glyphicon"><sup class="glyphicon-new-window"></sup></span>
                                        </a>`;
                    } else {
                        parsedData = data;
                    }

                    return parsedData;
                };
                break;
        }

        return defs;
    }

    function reorderCol(evt, col) {
        if ( evt.target === evt.currentTarget ) {
            col.order(evt.target.className.indexOf('sorting_desc') > -1 ? 'asc' : 'desc').draw();
        }
    }

    function hideBadge( badge, scope ) {
        if ( !badge || !scope ) return;
        scope.parents('thead').find( '.badge.' + badge ).removeClass( 'checked' );
    }

    function showBadge( badge, scope ) {
        if ( !badge || !scope ) return;
        hideBadge( badge, scope );
        scope.find( '.badge.' + badge ).addClass( 'checked' );
    }

    function labelClickHandler( evt, header ) {
        if ( $(evt.target).is('input') ) {
            let listItem = $(evt.target).parents('li');
            let list = $(evt.target).parents('ul');
            let radio = listItem.find(':radio');
            let name = radio.attr('name');

            if ( name === 'markerLatitude' && list.has('[name=markerLongitude]:checked' ).length ) {
                config.set('markerLongitude', header.find('[name=markerLongitude]').prop('checked', false).change());
            } else if ( name === 'markerLongitude' && list.has('[name=markerLatitude]:checked').length ) {
                config.set('markerLatitude', header.find('[name=markerLatitude]').prop('checked', false).change());
            }
            config.set(name, radio.val());
            radio.prop('checked', true);
        }
    }

    function insertColItems(idx) {
        let col = inner.tableApi.column(idx);

        if ( col.dataSrc() === 'visible' ) {
            inner.tpl.footer.append('<th></th>');
            return;
        }

        template.get('datatable.search', {title: col.dataSrc()})
            .then( tpl => {
                inner.tpl.footer
                    .append(tpl)
                    .find(tpl)
                    .on( 'keyup change', evt => {
                        let input = evt.target;
                        if ( col.search() !== input.value ) {
                            col.search( input.value ).draw();
                        }
                    });
            })
            .catch( e => console.log(e) );

        template.get(
            'datatable.headerOptions',
            {
                title: col.dataSrc(),
                lastCol: (idx === options.columns.length - 1 ? 'lastCol' : '' ),
                isMarkerLabel: config.get('markerLabel') === col.dataSrc() ? 'checked' : '',
                isMarkerLatitude: config.get('markerLatitude') === col.dataSrc() ? 'checked' : '',
                isMarkerLongitude: config.get('markerLongitude') === col.dataSrc() ? 'checked' : ''
            })
            .then( tpl => {
                let header = $(col.header());

                header
                    .append( tpl )
                    .off()
                    .on('click', evt => reorderCol(evt, col))
                    .find('.dropdown-menu')
                    .find('label')
                    .on('click', evt => labelClickHandler(evt, header))
                    .find(':radio')
                    .on('change', evt => {
                        evt.stopPropagation();
                        let name = $(evt.target).attr('name');
                        if ( $(evt.target).is(':checked') ) {
                            showBadge(name, header);
                        } else {
                            hideBadge(name, header);
                        }
                    });


            })
            .catch( e => console.log(e) );
    }

    function toggleVisibility(evt) {
        let cellEl = $(evt.currentTarget).parent('td'),
            cell = inner.tableApi.cell(cellEl),
            visible = !cell.data();
        cellEl.parent('tr')[visible?'removeClass':'addClass']('inactive');
        cell.data(visible);
    }

    template.get('markers.manager')
        .then( tpl => {
            function applyReDrawVisibility() {
                inner.tpl.tableElement.find('.glyphicon-eye-close').parents('tr').addClass('inactive');
            }

            $('#manage').append(tpl);
            inner.tpl.tableElement = $('#markersTable');
            inner.tpl.tableElement.on('init draw.dt', applyReDrawVisibility);
            inner.tpl.footer = inner.tpl.tableElement.find('tfoot tr');
            options.columns = gridData.cols.map( getCustomColDefinition );
            options.data = gridData.data;
            inner.tableApi = inner.tpl.tableElement.DataTable( options );
            inner.tableApi.columns().every(insertColItems);
            inner.tpl.tableElement.on('click', '.toggleVisibility', toggleVisibility);

            config.set('tableApi', inner.tableApi);

        })
        .catch( e => console.log(e) );

    return options;
}

module.exports = dataTable;