/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

var config = new Map();
config.set('mapId', 'map');
config.set('GMapApiKey', 'AIzaSyBpE0cAP--pypP4zdUNbJ2Np29zuly9NuQ');
config.set('mapElement', '');
config.set('templates', ['datatable.search', 'datatable.headerOptions', 'datatable', 'loadSpinner', 'markers.manager', 'markers', 'markersPane', 'markersPane.marker']);
config.set('csvMaxSize', 2 * 1024 * 1024);
config.set('csvMaxLines', 20);
config.set('gridImageSize', {width: 50, height: 50});
config.set('viewsPath', 'views/');

module.exports = config;