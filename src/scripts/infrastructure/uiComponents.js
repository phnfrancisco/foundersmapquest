/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

const template = require('./templateHandler');
const CustomPromise = require('./customPromise');
const loadSpinner = require('./uiComponents/loadSpinner');
const markersLoader = require('./uiComponents/markersLoader.js');
const markersPane = require('./uiComponents/markersPane');
const $ = global.$;

//fix to make bootstrap modal work properly with multiple modals
$.fn.modal.Constructor.prototype.hideModal = function () {
    var self = this;
    this.$element.hide();
    this.backdrop(function () {
        if ( !$('.modal-backdrop').length ) self.$body.removeClass('modal-open');
        self.resetAdjustments();
        self.resetScrollbar();
        self.$element.trigger('hidden.bs.modal');
    });
};

function preLoadComponents() {
    var promise = new CustomPromise();
    template.preLoad()
        .then( () => promise.resolve() )
        .catch( () => promise.reject() );
    return promise;
}

module.exports = {
    preLoad: preLoadComponents,
    loadSpinner: loadSpinner(),
    markersLoader: markersLoader(),
    markersPane: markersPane()
};