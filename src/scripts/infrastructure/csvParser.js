/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

const csvMaxLines = require('./config').get('csvMaxLines');

function csvParser(data, separator) {

    function markerDataClassFactory(attributes) {
        return class MarkerData {
            constructor (data) {
                attributes.forEach(attr => this[attr] = data[attr]);
            }
        };
    }

    var lines = data.split('\n');
    var cols = lines.shift().split(separator).map(attr => attr.trim());
    const colsLength = cols.length;
    if ( lines.length > csvMaxLines ) {
        lines.length = csvMaxLines;
    }
    lines = lines.map(entry => entry.split(separator));
    var objs = [];
    lines.forEach( line => {
        var obj = {};
        if ( line.length != colsLength ) {
            return;
        }
        cols.forEach((attr, idx) => {
            obj[attr.trim()] = line[idx];
        });
        objs.push(obj);
    });

    var MarkerData = markerDataClassFactory(cols);
    var markers = objs.map(obj => new MarkerData(obj));

    return {
        data: markers,
        cols: cols
    };
}


module.exports = csvParser;