'use strict';
const CustomPromise = require('../infrastructure/customPromise');
const request = require('../infrastructure/request');
const config = require('./config');
const $ = global.$;

function template() {

    let templateCache = {};

    function get(template, data) {
        let promise = new CustomPromise();

        if ( !template ) {
            promise.reject('Tryied to load a template without specifying which one.');
            return promise;
        }

        function applyData(tpl, data) {
            if ( data ) {
                for ( let key in data ) {
                    if ( !data.hasOwnProperty(key) ) continue;
                    tpl = tpl.replace(new RegExp("\\$\{" + key + "\}", "g"), data[key]);
                }
            }
            return tpl;
        }

        if( templateCache[template] ) {
            promise.resolve($.parseHTML(applyData(templateCache[template], data)));
            return promise;
        }

        request.get(config.get('viewsPath') + template + '.template.html')
            .then( tpl => {
                try {
                    templateCache[template] = tpl;
                    promise.resolve($.parseHTML(applyData(tpl, data)));
                } catch (e) {
                    promise.reject(e);
                }
            })
            .catch( e => promise.reject(e) );
        return promise;
    }

    function preLoad(templates) {
        var promise = new CustomPromise();
        switch (typeof templates) {
            case 'undefined' :
                templates = config.get('templates');
                break;
            case 'string':
                templates = [templates];
                break;
            case 'object':
                templates = typeof templates.push === 'function' ? templates : [];
                break;
            default:
                templates = [];
        }

        templates.forEach( (template, $idx) => {
            get(template)
                .then( () => {
                        if ( templates.length === $idx +1 ) promise.resolve();
                    } )
                .catch( e => console.log(e) );
        } );

        return promise;
    }

    return {
        get: get,
        preLoad: preLoad
    };
}

module.exports = template();
